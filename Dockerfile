FROM node:16-slim

ENV NODE_ENV=
ENV SERVER_BACKEND_HOST=
ENV SERVER_BACKEND_PORT=
ENV SERVER_BACKEND_NAME=
ENV SERVER_BACKEND_API_PREFIX=
ENV SERVER_BACKEND_PROTOCOL=
ENV SERVER_BACKEND_DB_URL=

RUN apt-get update
RUN apt-get install -y openssl

WORKDIR /app

RUN mkdir -p /app/dist

RUN yarn global add typescript

COPY . .
RUN yarn install --production && yarn prisma generate
# RUN yarn prisma migrate deploy
# RUN yarn ts-node -e "import {seedProd} from './db/seed/seed.prod';seedProd()"
RUN cp -r prisma /app/dist
RUN ln -s "$PWD/node_modules" /app/dist/node_modules
RUN tsc
RUN ln -s "$PWD/prisma/node_modules" /app/dist/prisma/node_modules
# RUN yarn ts-node -e "import {seedProd} from './db/seed/seed.prod';seedProd()"
RUN cp package.json yarn.lock /app/dist

WORKDIR /app/dist
EXPOSE 4000 
CMD ["node", "index.js"]
